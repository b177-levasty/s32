const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");


// Route checking if the user's email already exists in the database
// endpoint: localhost:4000/users/checkEmail
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for registering a user
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Activity
//1. Create a /details route that will accept the user’s Id to retrieve the details of a user.
/*router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getUserDetails({_id : userData._id}).then(resultFromController => res.send(resultFromController));
})*/

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});


// para makuha yung Id ni user
router.get("/", (req, res) => {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
});

// Route to enroll user to a course
// Activity - auth.verify lang po nilagay ko and nagana naman po
router.post("/enroll" , auth.verify,(req, res) => {
	let data ={
		userId : req.body.userId,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
})

module.exports = router;


